import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Image, ScrollView } from 'react-native';

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: null
        }
    }

    componentDidMount() {
        this.getProductsName()
    }



    getProductsName = () => {

        return fetch('http://vlad.mage2.interactivated.me/rest/default/V1/products?searchCriteria[pageSize]=10')
            .then( (response) => response.json() )
            .then( (responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson.items
                });
            })

            .catch((error) => {
                console.log(error)
            });
    }  ;


    render() {


        const mediaUrl = 'http://vlad.mage2.interactivated.me/media/catalog/product';

        if (this.state.isLoading) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator/>
                </View>
            )
        } else {

            let products = this.state.dataSource.map((item, key) => {
                const {media_gallery_entries, name} = item;
                return media_gallery_entries ? media_gallery_entries.map((mediaItem) => {
                    return (
                        <View key={key}>
                            <Text>{name}</Text>
                            {/*<Text>{mediaUrl}{mediaItem.file}</Text>*/}
                            <Image style={styles.imageStyle} source={{uri: {mediaUrl}{mediaItem.file}}}/>
                        </View>
                    )
                }) : null;
            });


            // пробовал венести перебор media_gallery_entries за View чтобы выводилось имя только раз, а не дублировалось
            // let products = this.state.dataSource.map((item, key) => {
            //     const {media_gallery_entries, name} = item;
            //     const image = media_gallery_entries.map((mediaItem) => {mediaItem.file});
            //     return (
            //         <View key={key}>
            //             <Text>{name}</Text>
            //             <Text>{image}</Text>
            //         </View>
            //     )
            // });

            return (
                <View style={styles.container}>
                    <ScrollView style={styles.scrollContainer}>
                    {products}
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollContainer: {
        flex: 1,
        marginTop: 20,
        marginBottom: 75
    },
    imageStyle: {
        width: 100,
        height: 100
    }
});
